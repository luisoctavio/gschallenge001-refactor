
# Galicia Seguros Coding Challenge Refactor

La solución está divida en tres proyectos:
- app.api
- app.data
- app.data.tests

```app.api``` contiene la API y modelos de datos
```app.data``` contiene los servicios y sus componentes
```app.data.test``` contiene las pruebas unitarias

## 💡 Características
-   [✔️] API
-   [✔️] Docker
-   [✔️] Pruebas unitarias
-   [✔️] Soporte para idiomas: Español, Ingles y Francés

## 💡 Instalación con Docker

1. Cloná el repositorio:
```git clone https://bitbucket.org/luisoctavio/gschallenge001-refactor.git```
2. Abrí el directorio con la solución e inicia una consola de comandos como Administrador
3. Ejecuta ``` cd app.api && docker build -f Dockerfile .. -t galicia-challenge```
4. ¡Listo!

## 💡 Como ejecutar la API con Docker
1. Ejecuta la solucion con docker ```docker run -dp 44300:80 galicia-challenge```
2. Ingresa a la siguiente dirección: https://localhost:44300

## 💡 Como usar la API
1. Para obtener la lista de idiomas soportados
``` https://localhost:44300/languages ```

2. Para generar un reporte hacer una Request POST al siguiente endpoint:
``` https://localhost:44300/reports ```
con el siguiente modelo de referencia en el body:
```
{
  "language": "string",
  "shapes": {
    "circles": [
      {
        "radius": 0
      }
    ],
    "pentagons": [
      {
        "side": 0
      }
    ],
    "squares": [
      {
        "side": 0
      }
    ],
    "triangles": [
      {
        "baseLength": 0
      }
    ],
    "rectangles": [
      {
        "longerSide": 0,
        "shorterSide": 0
      }
    ]
  }
}
```

Se debe introducir un idioma válido, de lo contrario, el reporte devolverá el siguiente error:
```"No se encontró el lenguaje 'lenguaje invalido'."```

La lista de lenguajes válidos se puede obtener desde el endpoint de idiomas soportados: ``` GET https://localhost:44300/languages ```



## Valores inválidos ##
En el caso de que uno de los valores sea inválido, por ejemplo un círculo dónde su  radio (```radius```) sea  ```-1```, obtendrá lo siguiente:
```"invalid value: '-1', shape: 'x' (Parameter 'x')"``` 

### En caso de un rectángulo inválido ###
Si ```longerSide``` es menor que ```shorterSide```, obtendrá lo siguiente:
```"'longerSide'(x) no puede ser menor a 'shorterSide'(x), shape: 'Rectangle'"``` donde x representa el lado más largo y el lado más corto respectivamente.
