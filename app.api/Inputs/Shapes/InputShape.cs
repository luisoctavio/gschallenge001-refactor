﻿using app.data.Shapes;
using System.Collections.Generic;
using System.Linq;

namespace app.data
{
    public class InputShape
    {
        public List<CircleInput> Circles { get; set; }
        public List<PentagonInput> Pentagons { get; set; }
        public List<SquareInput> Squares { get; set; }
        public List<TriangleInput> Triangles { get; set; }
        public List<RectangleInput> Rectangles { get; set; }
        private List<Shape> AllShapes { get; set; } = new List<Shape>();

        public List<Shape> GetAllShapes()
        {
            if (!(Circles is null))
                AllShapes.AddRange(Circles.Select(x => new Circle(x.Radius)));

            if (!(Pentagons is null))
                AllShapes.AddRange(Pentagons.Select(x => new Pentagon(x.Side)));

            if (!(Squares is null))
                AllShapes.AddRange(Squares.Select(x => new Square(x.Side)));

            if (!(Triangles is null))
                AllShapes.AddRange(Triangles.Select(x => new Triangle(x.BaseLength)));

            if (!(Rectangles is null))
                AllShapes.AddRange(Rectangles.Select(x => new Rectangle(x.LongerSide, x.ShorterSide)));

            return AllShapes;
        }
    }
}