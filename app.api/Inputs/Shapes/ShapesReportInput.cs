﻿namespace app.data
{
    public class ShapesReportInput
    {
        public string Language { get; set; }

        public InputShape Shapes { get; set; }
    }
}