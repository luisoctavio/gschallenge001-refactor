﻿namespace app.data
{
    public class CircleInput
    {
        public double Radius { get; set; }
    }
}