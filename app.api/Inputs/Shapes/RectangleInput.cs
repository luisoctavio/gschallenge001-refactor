﻿namespace app.data
{
    public class RectangleInput
    {
        public double LongerSide { get; set; }
        public double ShorterSide { get; set; }
    }
}