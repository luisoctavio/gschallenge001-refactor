﻿using app.data;
using Microsoft.AspNetCore.Mvc;
using System;

namespace app.api.Controllers
{
    [Route("/reports/")]
    public class ReportsController : Controller
    {
        [HttpPost]
        public IActionResult Index([FromBody] ShapesReportInput model)
        {
            ReportService reportService = new ReportService();
            string result;
            int status = 200;

            try
            {
                result = reportService.GetReport(model.Language, model.Shapes.GetAllShapes());
            }
            catch (Exception ex)
            {
                result = ex.Message;
                status = 500;
            }

            return new JsonResult(result)
            {
                StatusCode = status,
            };
        }
    }
}