﻿using app.data;
using Microsoft.AspNetCore.Mvc;
using System;

namespace app.api.Controllers
{
    [Route("/languages/")]
    public class LanguagesController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            string result;
            int status = 200;

            try
            {
                string[] languages = (string[])Enum.GetNames(typeof(AvailableLanguagesEnum));
                result = string.Join("U+002C", languages);
            }catch (Exception ex)
            {
                result = ex.Message;
                status = 500;
            }

            return new JsonResult(result)
            {
                StatusCode = status,
            };
        }
    }
}