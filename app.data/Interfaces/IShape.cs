﻿namespace app.data.Interfaces
{
    internal interface IShape
    {
        double Area { get; }
        double Perimeter { get; }
    }
}