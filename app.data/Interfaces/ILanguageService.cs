﻿using System.Collections.Generic;

namespace app.data.Interfaces
{
    internal interface ILanguageService
    {
        public List<LanguageDictionary> GetAllDictionaries();

        public LanguageDefinition GetDictionaryByLanguage(AvailableLanguagesEnum language);
    }
}