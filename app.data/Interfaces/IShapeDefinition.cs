﻿namespace app.data.Interfaces
{
    public interface IShapeDefinition
    {
        public string Singular { get; set; }
        public string Plural { get; set; }
    }
}