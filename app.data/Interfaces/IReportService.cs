﻿using System.Collections.Generic;

namespace app.data.Interfaces
{
    internal interface IReportService
    {
        public string GetReport(string languageInput, List<Shape> shapes);
    }
}