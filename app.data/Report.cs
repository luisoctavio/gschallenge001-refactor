﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace app.data
{
    public class Report
    {
        private StringBuilder _report;
        private readonly LanguageService _languageService = new LanguageService();
        private readonly IList<Shape> _shapes;
        private readonly LanguageDefinition Definitions;

        private IEnumerable<ShapeOutput> Shapes
        {
            get
            {
                return _shapes.GroupBy(x => x.GetType()).Select(y => new ShapeOutput
                {
                    Shape = y.First().GetType(),
                    Count = y.Count(),
                    Area = (decimal)y.Sum(x => x.Area),
                    Perimeter = (decimal)y.Sum(x => x.Perimeter)
                });
            }
        }

        private int ShapesCount => Shapes.Sum(_ => _.Count);
        private bool HasMany => ShapesCount > 0;
        private decimal ShapesTotalArea => Shapes.Sum(_ => _.Area);
        private decimal ShapesTotalPerimeter => Shapes.Sum(_ => _.Perimeter);

        public Report(IList<Shape> items, AvailableLanguagesEnum language)
        {
            Definitions = _languageService.GetDictionaryByLanguage(language);
            if (items != null)
                _shapes = items;
        }

        public string Print()
        {
            if (_shapes is null)
                return Definitions.NullShapeReport;

            if (!Shapes.Any())
                return Definitions.EmptyShapeReport;

            BuildReport();
            return _report.ToString();
        }

        private void BuildReport()
        {
            _report = new StringBuilder();

            _report.AppendLine(Definitions.ShapeReport);

            foreach (var shape in Shapes)
            {
                var shapeDef = Definitions
                    .ShapeDefinitions
                    .Find(x => x.GetType().FullName.Contains(shape.Shape.Name));

                var shapeName = shape.Count > 1 ? shapeDef.Plural : shapeDef.Singular;

                _report
                    .Append($"{shape.Count} {shapeName}")
                    .Append(" | ")
                    .Append($"{Definitions.Area} {shape.Area}")
                    .Append(" | ")
                    .Append($"{Definitions.Perimeter} {shape.Perimeter}")
                    .AppendLine();
            }

            _report
                .Append($"{Definitions.Total.ToUpper()}: {ShapesCount} {(HasMany ? Definitions.Plural : Definitions.Singular)}")
                .Append(' ')
                .Append($"{Definitions.Perimeter} {ShapesTotalPerimeter} {Definitions.Area} {ShapesTotalArea}");
        }
    }
}