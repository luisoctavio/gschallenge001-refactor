﻿/*
 * Realizar refactor respetando principios de programación orientada a objetos.
 * ¿Qué sucede si es necesario soportar un nuevo idioma para los reportes o agregar más formas geométricas?
 *
 * Cualquier cambio es permitido tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Pentágono/Rectangulo, agregar otro idioma a reporting.
*/

using app.data.Interfaces;

namespace app.data
{
    public abstract class Shape : IShape
    {
        public abstract double Area { get; }
        public abstract double Perimeter { get; }
    }
}