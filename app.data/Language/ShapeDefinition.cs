﻿using app.data.Interfaces;

namespace app.data
{
    public class ShapeDefinition<TShape> : IShapeDefinition where TShape : Shape
    {
        public string Plural { get; set; }
        public string Singular { get; set; }
    }
}