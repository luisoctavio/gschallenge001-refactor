﻿namespace app.data
{
    public enum AvailableLanguagesEnum
    {
        English,
        Spanish,
        French
    }
}