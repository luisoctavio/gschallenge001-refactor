﻿using app.data.Interfaces;
using System.Collections.Generic;

namespace app.data
{
    public class LanguageDefinition
    {
        public string EmptyShapeReport { get; set; }
        public string NullShapeReport { get; set; }
        public string ShapeReport { get; set; }
        public string Area { get; set; }
        public string Perimeter { get; set; }
        public string Total { get; set; }
        public string Singular { get; set; }
        public string Plural { get; set; }
        public List<IShapeDefinition> ShapeDefinitions { get; set; }
    }
}