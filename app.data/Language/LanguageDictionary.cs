﻿namespace app.data
{
    public class LanguageDictionary
    {
        public AvailableLanguagesEnum LanguageName { get; set; }
        public LanguageDefinition Definitions { get; set; }
    }
}