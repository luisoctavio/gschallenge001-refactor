﻿using System;

namespace app.data
{
    public class ShapeOutput
    {
        public Type Shape { get; set; }
        public int Count { get; set; }
        public decimal Area { get; set; }
        public decimal Perimeter { get; set; }
    }
}