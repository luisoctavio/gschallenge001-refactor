﻿using System;

namespace app.data.Shapes
{
    public class Triangle : Shape
    {
        private double _base;

        public Triangle(double base_length) : base()
        {
            if (base_length < 0)
                throw new ArgumentOutOfRangeException(nameof(base_length), $"invalid value: '{base_length}', shape: '{typeof(Triangle).Name}'");

            _base = base_length;
        }

        public override double Perimeter => _base * 3;

        public override double Area => Math.Sqrt(3) / 4 * (_base * _base);
    }
}