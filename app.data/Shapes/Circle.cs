﻿using System;

namespace app.data.Shapes
{
    public class Circle : Shape
    {
        private double _radius;

        public Circle(double radius)
        {
            if (radius < 0)
                throw new ArgumentOutOfRangeException(nameof(radius), $"invalid value: '{radius}', shape: '{typeof(Circle).Name}'");

            _radius = radius;
        }

        public override double Perimeter => Math.PI * (_radius * _radius);

        public override double Area => 2 * Math.PI * _radius;
    }
}