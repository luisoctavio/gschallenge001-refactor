﻿using System;

namespace app.data.Shapes
{
    public class Pentagon : Shape
    {
        private double _side;
        private double _apothem => _side / (2 * Math.Tan(36));

        public Pentagon(double side)
        {
            if (side < 0)
                throw new ArgumentOutOfRangeException(nameof(side), $"invalid value: '{side}', shape: '{typeof(Pentagon).Name}'");

            _side = side;
        }

        public override double Perimeter => _side * 5;

        public override double Area => Perimeter * _apothem / 2;
    }
}