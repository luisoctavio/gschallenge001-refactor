﻿using System;

namespace app.data.Shapes
{
    public class Rectangle : Shape
    {
        private double _longerSide;
        private double _shorterSide;

        public Rectangle(double longerSide, double shorterSide)
        {
            if (longerSide < shorterSide)
                throw new ArgumentException($"'{nameof(longerSide)}'({longerSide}) no puede ser menor a '{nameof(shorterSide)}'({shorterSide}), shape: '{typeof(Rectangle).Name}'");

            _longerSide = longerSide;
            _shorterSide = shorterSide;
        }

        public override double Perimeter => (2 * _shorterSide) + (2 * _longerSide);

        public override double Area => _shorterSide * _longerSide;
    }
}