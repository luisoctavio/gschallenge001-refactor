﻿using System;

namespace app.data.Shapes
{
    public class Square : Shape
    {
        private double _side;

        public Square(double side)
        {
            if (side < 0)
                throw new ArgumentOutOfRangeException(nameof(side), $"invalid value: '{side}', shape: '{typeof(Square).Name}'");

            _side = side;
        }

        public override double Perimeter => _side * 4;
        public override double Area => _side * _side;
    }
}