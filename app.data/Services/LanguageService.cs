﻿using app.data.Interfaces;
using app.data.Shapes;
using System.Collections.Generic;
using System.Linq;

namespace app.data
{
    public class LanguageService : ILanguageService
    {
        public List<LanguageDictionary> GetAllDictionaries()
        {
            return new List<LanguageDictionary>()
            {
                new LanguageDictionary()
                {
                    LanguageName = AvailableLanguagesEnum.English,
                    Definitions = new LanguageDefinition()
                    {
                        EmptyShapeReport = "Empty list of shapes!",
                        NullShapeReport = "Null list of shapes!",
                        ShapeReport = "Shapes report",
                        Area = "Area",
                        Perimeter = "Perimeter",
                        Total = "Total",
                        Singular = "Shape",
                        Plural = "Shapes",
                        ShapeDefinitions = new List<IShapeDefinition>()
                        {
                            new ShapeDefinition<Square> { Plural = "Squares", Singular = "Square" },
                            new ShapeDefinition<Triangle> { Plural = "Triangles", Singular = "Triangle" },
                            new ShapeDefinition<Circle> { Plural = "Círcles", Singular = "Circle" },
                            new ShapeDefinition<Pentagon> { Plural = "Pentagons", Singular = "Pentagon" },
                            new ShapeDefinition<Rectangle> { Plural = "Rectangles", Singular = "Rectangle" }
                        }
                    }
                },
                new LanguageDictionary()
                {
                    LanguageName = AvailableLanguagesEnum.Spanish,
                    Definitions = new LanguageDefinition()
                    {
                        EmptyShapeReport = "Reporte vacío de formas!",
                        NullShapeReport = "Reporte nulo de formas!",
                        ShapeReport = "Reporte de formas",
                        Area = "Área",
                        Perimeter = "Perímetro",
                        Total = "Total",
                        Singular = "Forma",
                        Plural = "Formas",
                        ShapeDefinitions = new List<IShapeDefinition>()
                        {
                            new ShapeDefinition<Square> { Plural = "Cuadrados", Singular = "Cuadrado" },
                            new ShapeDefinition<Triangle> { Plural = "Triángulos", Singular = "Triángulo" },
                            new ShapeDefinition<Circle> { Plural = "Círculos", Singular = "Círculo" },
                            new ShapeDefinition<Pentagon> { Plural = "Pentágonos", Singular = "Pentágono" },
                            new ShapeDefinition<Rectangle> { Plural = "Rectángulos", Singular = "Rectángulo" }
                        }
                    }
                },
                new LanguageDictionary()
                {
                    LanguageName = AvailableLanguagesEnum.French,
                    Definitions = new LanguageDefinition()
                    {
                        EmptyShapeReport = "Rapport vide sur les formes!",
                        NullShapeReport = "Rapport nul sur les formes!",
                        ShapeReport = "Rapport sur les formes",
                        Area = "Zone",
                        Perimeter = "Périmètre",
                        Total = "Le total",
                        Singular = "Forme",
                        Plural = "Formes",
                        ShapeDefinitions = new List<IShapeDefinition>()
                        {
                            new ShapeDefinition<Square> { Plural = "Carrés", Singular = "Carré" },
                            new ShapeDefinition<Triangle> { Plural = "Triangles", Singular = "Triangle" },
                            new ShapeDefinition<Circle> { Plural = "Cercles", Singular = "Cercle" },
                            new ShapeDefinition<Pentagon> { Plural = "Pentagones", Singular = "Pentagone" },
                            new ShapeDefinition<Rectangle> { Plural = "Rectangles", Singular = "Rectangle" }
                        }
                    }
                }
            };
        }

        public LanguageDefinition GetDictionaryByLanguage(AvailableLanguagesEnum language)
        {
            return this.GetAllDictionaries().FirstOrDefault(x => x.LanguageName == language).Definitions;
        }
    }
}