﻿using app.data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace app.data
{
    public class ReportService : IReportService
    {
        public string GetReport(string languageInput, List<Shape> shapes)
        {
            string result;

            try
            {
                if (string.IsNullOrEmpty(languageInput))
                    throw new Exception($"El lenguaje especificado está vacío. Por favor especifique un lenguaje válido.");

                IEnumerable<string> Languages = Enum.GetNames(typeof(AvailableLanguagesEnum)).AsEnumerable();

                if (!Languages.Any(x => x.Equals(languageInput, StringComparison.InvariantCultureIgnoreCase)))
                    throw new Exception($"No se encontró el lenguaje '{languageInput}'.");

                AvailableLanguagesEnum language = (AvailableLanguagesEnum)Enum.Parse(typeof(AvailableLanguagesEnum), languageInput);

                result = new Report(shapes, language).Print();
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
    }
}