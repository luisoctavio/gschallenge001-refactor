﻿using app.data.Shapes;
using System;
using System.Collections.Generic;
using Xunit;

namespace app.data.tests
{
    public class FigureTest
    {
        #region Inherent of language definitions

        [Fact]
        public void EmptyInputLanguage()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "El lenguaje especificado está vacío. Por favor especifique un lenguaje válido.";

            try
            {
                result = reportService.GetReport("", new List<Shape>
                {
                    new Square(2)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void NullInputLanguage()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "El lenguaje especificado está vacío. Por favor especifique un lenguaje válido.";

            try
            {
                result = reportService.GetReport(null, new List<Shape>
                {
                    new Square(2)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void InvalidReportInputLanguage()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "No se encontró el lenguaje 'Chinese'.";

            try
            {
                result = reportService.GetReport("Chinese", new List<Shape>
                {
                    new Square(2)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        #endregion Inherent of language definitions

        #region Empty list of shapes

        [Fact]
        public void EmptyListOfShapesInEnglish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Empty list of shapes!";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void EmptyListOfShapesInSpanish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Reporte vacío de formas!";

            try
            {
                result = reportService.GetReport("Spanish", new List<Shape>
                {
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void EmptyListOfShapesInFrench()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Rapport vide sur les formes!";

            try
            {
                result = reportService.GetReport("French", new List<Shape>
                {
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        #endregion Empty list of shapes

        #region Null list of shapes

        [Fact]
        public void NullListOfShapesInEnglish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Null list of shapes!";

            try
            {
                result = reportService.GetReport("English", null);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void NullListOfShapesInSpanish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Reporte nulo de formas!";

            try
            {
                result = reportService.GetReport("Spanish", null);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void NullListOfShapesInFrench()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Rapport nul sur les formes!";

            try
            {
                result = reportService.GetReport("French", null);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        #endregion Null list of shapes

        #region List with one of each shape

        [Fact]
        public void TestPrintOneOfEachEnglish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Shapes report\r\n1 Square | Area 4 | Perimeter 8\r\n1 Triangle | Area 3,89711431702997 | Perimeter 9\r\n1 Circle | Area 25,1327412287183 | Perimeter 50,2654824574367\r\n1 Pentagon | Area 4,03201307123429 | Perimeter 25\r\n1 Rectangle | Area 18 | Perimeter 18\r\nTOTAL: 5 Shapes Perimeter 110,2654824574367 Area 55,06186861698256";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Square(2),
                    new Triangle(3),
                    new Circle(4),
                    new Pentagon(5),
                    new Rectangle(6,3)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestPrintOneOfEachSpanish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Reporte de formas\r\n1 Cuadrado | Área 4 | Perímetro 8\r\n1 Triángulo | Área 3,89711431702997 | Perímetro 9\r\n1 Círculo | Área 25,1327412287183 | Perímetro 50,2654824574367\r\n1 Pentágono | Área 4,03201307123429 | Perímetro 25\r\n1 Rectángulo | Área 18 | Perímetro 18\r\nTOTAL: 5 Formas Perímetro 110,2654824574367 Área 55,06186861698256";

            try
            {
                result = reportService.GetReport("Spanish", new List<Shape>
                {
                    new Square(2),
                    new Triangle(3),
                    new Circle(4),
                    new Pentagon(5),
                    new Rectangle(6,3)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestPrintOneOfEachFrench()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Rapport sur les formes\r\n1 Carré | Zone 4 | Périmètre 8\r\n1 Triangle | Zone 3,89711431702997 | Périmètre 9\r\n1 Cercle | Zone 25,1327412287183 | Périmètre 50,2654824574367\r\n1 Pentagone | Zone 4,03201307123429 | Périmètre 25\r\n1 Rectangle | Zone 18 | Périmètre 18\r\nLE TOTAL: 5 Formes Périmètre 110,2654824574367 Zone 55,06186861698256";

            try
            {
                result = reportService.GetReport("French", new List<Shape>
                {
                    new Square(2),
                    new Triangle(3),
                    new Circle(4),
                    new Pentagon(5),
                    new Rectangle(6,3)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        #endregion List with one of each shape

        #region List with mixed shapes shape

        [Fact]
        public void TestPrintMixedEnglish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Shapes report\r\n3 Squares | Area 36 | Perimeter 40\r\n2 Triangles | Area 8,3311643844063 | Perimeter 18,6\r\n3 Círcles | Area 196,035381584003 | Perimeter 1575,32022021607\r\n1 Pentagon | Area 0 | Perimeter 0\r\n1 Rectangle | Area 75 | Perimeter 37\r\nTOTAL: 10 Shapes Perimeter 1670,92022021607 Area 315,3665459684093";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Square(2),
                    new Triangle(3.2),
                    new Circle(1.2),
                    new Pentagon(0),
                    new Rectangle(12.5,6),
                    new Square(4),
                    new Triangle(3),
                    new Square(4),
                    new Circle(20),
                    new Circle(10)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestPrintMixedSpanish()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Reporte de formas\r\n4 Cuadrados | Área 64 | Perímetro 64\r\n2 Triángulos | Área 7,79422863405995 | Perímetro 18\r\n2 Círculos | Área 188,495559215388 | Perímetro 1570,7963267949\r\nTOTAL: 8 Formas Perímetro 1652,7963267949 Área 260,28978784944795";

            try
            {
                result = reportService.GetReport("Spanish", new List<Shape>
                {
                    new Square(4),
                    new Triangle(3),
                    new Square(4),
                    new Circle(20),
                    new Circle(10),
                    new Square(4),
                    new Triangle(3),
                    new Square(4)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestPrintMixedFrench()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "Rapport sur les formes\r\n2 Carrés | Zone 32 | Périmètre 32\r\n1 Triangle | Zone 3,89711431702997 | Périmètre 9\r\n2 Cercles | Zone 188,495559215388 | Périmètre 1570,7963267949\r\n1 Rectangle | Zone 51 | Périmètre 30,4\r\nLE TOTAL: 6 Formes Périmètre 1642,1963267949 Zone 275,39267353241797";

            try
            {
                result = reportService.GetReport("French", new List<Shape>
                {
                    new Square(4),
                    new Triangle(3),
                    new Square(4),
                    new Circle(20),
                    new Circle(10),
                    new Rectangle(10.2,5)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        #endregion List with mixed shapes shape

        #region Invalid values of each figure

        [Fact]
        public void TestInvalidRectangle()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "'longerSide'(0) no puede ser menor a 'shorterSide'(10), shape: 'Rectangle'";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Rectangle(0,10)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestInvalidCircle()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "invalid value: '-1', shape: 'Circle' (Parameter 'radius')";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Circle(-1)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestInvalidSquare()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "invalid value: '-1', shape: 'Square' (Parameter 'side')";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Square(-1)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestInvalidPentagon()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "invalid value: '-1', shape: 'Pentagon' (Parameter 'side')";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Pentagon(-1)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        [Fact]
        public void TestInvalidTriangle()
        {
            ReportService reportService = new ReportService();
            string result;
            string expected_result = "invalid value: '-1', shape: 'Triangle' (Parameter 'base_length')";

            try
            {
                result = reportService.GetReport("English", new List<Shape>
                {
                    new Triangle(-1)
                });
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            Assert.Equal(expected_result, result);
        }

        #endregion Invalid values of each figure
    }
}